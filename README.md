# service_management_app


## Getting Started

Install Google Flutter at https://flutter.dev/docs/get-started/install

The main file is located at `lib/main.dart`

Run the application for iOS
![alt text](docs/images/ios_app.png "iOS App")


This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

https://appicon.co/
https://icons8.com/


