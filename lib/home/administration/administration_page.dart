import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:service_management_app/home/customers/customers_page.dart';
import 'package:service_management_app/home/employees/employees_page.dart';
import 'dart:convert';
import '../../models/Service.dart';
import '../services/services_page.dart';

class AdministrationScreen extends StatelessWidget {
  const AdministrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Administration'),
        // actions: <Widget>[
        //   Padding(
        //       padding: EdgeInsets.only(right: 20.0),
        //       child: GestureDetector(
        //         onTap: () {
        //           Navigator.push(
        //             context,
        //             MaterialPageRoute(builder: (context) => ServiceAddScreen()),
        //           );
        //         },
        //         child: Icon(
        //           Icons.add,
        //           size: 26.0,
        //         ),
        //       )
        //   ),
        // ],
      ),
      body: BodyLayout(),
    );
  }
}

class BodyLayout extends StatefulWidget {
  @override
  createState() => _MyListScreenState();

// @override
// Widget build(BuildContext context) {
//   return _myListView(context);
// }
}

class _MyListScreenState extends State {
  var _services = <Service>[];

  _getService() {
    fetchServices().then((services) {
      setState(() {
        _services = services;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getService();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        ListTile(
          title: Text("Service Management"),
          leading: Icon(Icons.settings),
          trailing: Icon(Icons.keyboard_arrow_right),
          contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          onTap: () {
            Navigator.of(context, rootNavigator: false).push(
              // context,
              MaterialPageRoute(
                  builder: (context) => ServiceScreen(),
                  fullscreenDialog: false),
            );
          },
        ),
        ListTile(
          title: Text("Customer Management"),
          leading: Icon(Icons.people),
          trailing: Icon(Icons.keyboard_arrow_right),
          contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => CustomerScreen()),
            );
          },
        ),
        ListTile(
          title: Text("Employee Management"),
          leading: Icon(Icons.engineering),
          trailing: Icon(Icons.keyboard_arrow_right),
          contentPadding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => EmployeeScreen()),
            );
          },
        ),
      ],
    );
  }
}

Future<List<Service>> fetchServices() async {
  List<Service> services = [];

  String apiEndpoint = "http://localhost:8080/api/services";

  final response = await http.get(Uri.parse(apiEndpoint));

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print(response.body);

    List<dynamic> list = json.decode(response.body);

    for (var i = 0; i < list.length; i++) {
      services.add(Service.fromJson(list[i]));
    }

    return services;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}
