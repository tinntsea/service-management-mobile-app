import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:service_management_app/models/Employee.dart';
import 'dart:convert';
import '../../models/Appointment.dart';
import 'package:flutter/services.dart';
import 'package:loader_overlay/loader_overlay.dart';
import '../../models/Service.dart';
import '../../models/Customer.dart';
// import 'package:dropdown_search/dropdown_search.dart';
// import 'package:searchable_dropdown/searchable_dropdown.dart';
import 'package:search_choices/search_choices.dart';
import 'dart:ui' as ui;

class AppointmentAddScreen extends StatefulWidget {
  static Future<String?> show(BuildContext context) async {
    return await Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => AppointmentAddScreen(),
      fullscreenDialog: true,
    ));
  }

  @override
  _AppointmentAddScreenState createState() => _AppointmentAddScreenState();
}

class _AppointmentAddScreenState extends State<AppointmentAddScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add a new appointment'),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: LoaderOverlay(
        // useDefaultLoading: false,
        // overlayWidget: Center(
        //   child: SpinKitCubeGrid(
        //     color: Colors.red,
        //     size: 50.0,
        //   ),
        // ),
        // overlayOpacity: 0.8,
        child: MyCustomForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  final _nameInputController = TextEditingController();
  final _priceInputController = TextEditingController();

  List<Service> _services = [];
  List<Customer> _customers = [];
  List<Employee> _employees = [];
  String _selectedLocation = 'A'; // Option 2

  int? _serviceId;
  int? _customerId;
  int? _employeeId;
  DateTime _datetime = DateTime.now();

  bool _isLoaderVisible = false;

  _fetchServices() {
    fetchServices().then((services) {
      setState(() {
        _services = services;
      });
    });
  }

  _fetchCustomers() {
    fetchCustomers().then((data) {
      setState(() {
        _customers = data;
      });
    });
  }

  _fetchEmployees() {
    fetchEmployees().then((data) {
      setState(() {
        _employees = data;
      });
    });
  }

  // getData(filter) {
  //   return _services;
  // }

  @override
  void initState() {
    super.initState();
    _fetchServices();
    _fetchCustomers();
    _fetchEmployees();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _nameInputController.dispose();
    _priceInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(
      padding: EdgeInsets.symmetric(horizontal: 140, vertical: 16),
      textStyle: const TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(28),
      ),
    );

    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: DropdownButtonFormField<int>(
              // hint: Text('Please choose a location'), // Not necessary for Option 1
              // value: _selectedLocation,
              onChanged: (int? newValue) {
                setState(() {
                  // _selectedLocation = newValue;
                  _serviceId = newValue;
                });
              },
              items: _services.map((Service service) {
                return DropdownMenuItem<int>(
                  child: new Text(service.name),
                  value: service.id,
                );
              }).toList(),
              decoration: const InputDecoration(
                hintText: 'Choose a service',
                // prefixIcon: Icon(Icons.info_outlined),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: DropdownButtonFormField<int>(
              // hint: Text('Please choose a location'), // Not necessary for Option 1
              // value: _selectedLocation,
              onChanged: (int? newValue) {
                setState(() {
                  // _selectedLocation = newValue;
                  _customerId = newValue;
                });
              },
              items: _customers.map((Customer customer) {
                return DropdownMenuItem<int>(
                  child: new Text(customer.fullName),
                  value: customer.id,
                );
              }).toList(),
              decoration: const InputDecoration(
                hintText: 'Choose a customer',
                // prefixIcon: Icon(Icons.info_outlined),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: DropdownButtonFormField<int>(
              // hint: Text('Please choose a location'), // Not necessary for Option 1
              // value: _selectedLocation,
              onChanged: (int? newValue) {
                setState(() {
                  // _selectedLocation = newValue;
                  _employeeId = newValue;
                });
              },
              items: _employees.map((Employee employee) {
                return DropdownMenuItem<int>(
                  child: new Text(employee.fullName),
                  value: employee.id,
                );
              }).toList(),
              decoration: const InputDecoration(
                hintText: 'Choose a employee',
                // prefixIcon: Icon(Icons.info_outlined),
              ),
            ),
          ),
          SizedBox(
            child: GestureDetector(
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 8.0, vertical: 5.0),
                padding:
                    const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black54,
                      width: 1.0,
                    ),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.event_outlined,
                      color: Colors.black87,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 0.0),
                    ),
                    Text(
                      DateFormat('yyyy/MM/dd – hh:mm a').format(_datetime),
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.black87,
                      ),
                    ),
                  ],
                ),
              ),
              onTap: () {
                DatePicker.showDateTimePicker(
                  context,
                  showTitleActions: true,
                  onChanged: (date) {
                    print('change $date in time zone ' +
                        date.timeZoneOffset.inHours.toString());
                  },
                  onConfirm: (date) {
                    print('confirm $date');
                    setState(() {
                      _datetime = date;
                    });
                  },
                  currentTime: _datetime,
                  theme: const DatePickerTheme(
                    backgroundColor: Colors.white,
                  ),
                );
              },
            ),
          ),
          // TextButton(
          //   onPressed: () {
          //     DatePicker.showDateTimePicker(
          //       context,
          //       showTitleActions: true,
          //       onChanged: (date) {
          //         print('change $date in time zone ' +
          //             date.timeZoneOffset.inHours.toString());
          //       },
          //       onConfirm: (date) {
          //         print('confirm $date');
          //         setState(() {
          //           _datetime = date;
          //         });
          //       },
          //       currentTime: _datetime,
          //       theme: const DatePickerTheme(
          //         backgroundColor: Colors.white,
          //       ),
          //     );
          //   },
          //   child: Text(
          //     "Time: " + DateFormat('yyyy-MM-dd – hh:mm').format(_datetime),
          //     style: TextStyle(
          //       color: Colors.black,
          //       // decoration: TextDecoration.underline,
          //     ),
          //   ),
          // ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
            child: Center(
              child: ElevatedButton(
                style: style,
                onPressed: () async {
                  // Validate returns true if the form is valid, or false otherwise.
                  if (_formKey.currentState!.validate()) {
                    context.loaderOverlay.show();
                    setState(() {
                      _isLoaderVisible = context.loaderOverlay.visible;
                    });

                    await createAppointment(
                      _serviceId!.toInt(),
                      _customerId!.toInt(),
                      _employeeId!.toInt(),
                      _datetime,
                    );
                    await Future.delayed(Duration(seconds: 1));

                    if (_isLoaderVisible) {
                      context.loaderOverlay.hide();
                    }
                    setState(() {
                      _isLoaderVisible = context.loaderOverlay.visible;
                    });
                    // If the form is valid, display a snackbar. In the real world,
                    // you'd often call a server or save the information in a database.
                    // ScaffoldMessenger.of(context).showSnackBar(
                    //   const SnackBar(content: Text('Processing Data')),
                    // );

                    ScaffoldMessenger.of(context)
                        .showSnackBar(
                          const SnackBar(
                            content: Text('New appointment has been created.'),
                            duration: const Duration(milliseconds: 500),
                            backgroundColor: Colors.green,
                          ),
                        )
                        .closed
                        .then((_) => Navigator.of(context).pop("test"));
                  }
                },
                child: const Text('Submit'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<Appointment> createAppointment(
  int serviceId,
  int customerId,
  int employeeId,
  DateTime datetime,
) async {
  Map data = {
    'serviceId': serviceId,
    'customerId': customerId,
    'employeeId': employeeId,
    'fromTime': datetime.millisecondsSinceEpoch,
  };

  String apiEndpoint = "http://localhost:8080/api/appointments";

  final response = await http.post(
    Uri.parse(apiEndpoint),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(data),
  );

  print("Calling to " + apiEndpoint);
  print(data);
  print(datetime);

  if (response.statusCode == 201 || response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    print(response.body);

    return Appointment.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to create appointment.');
  }
}

Future<List<Service>> fetchServices() async {
  List<Service> services = [];

  String apiEndpoint = "http://localhost:8080/api/services";

  final response = await http.get(Uri.parse(apiEndpoint));

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    List<dynamic> list = json.decode(response.body);

    for (var i = 0; i < list.length; i++) {
      services.add(Service.fromJson(list[i]));
    }

    return services;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

Future<List<Customer>> fetchCustomers() async {
  List<Customer> customers = [];

  String apiEndpoint = "http://localhost:8080/api/customers";

  final response = await http.get(Uri.parse(apiEndpoint));

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print(response.body);
    List<dynamic> list = json.decode(response.body);

    for (var i = 0; i < list.length; i++) {
      customers.add(Customer.fromJson(list[i]));
    }

    return customers;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

Future<List<Employee>> fetchEmployees() async {
  List<Employee> employees = [];

  String apiEndpoint = "http://localhost:8080/api/employees";

  final response = await http.get(Uri.parse(apiEndpoint));

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print(response.body);
    List<dynamic> list = json.decode(response.body);

    for (var i = 0; i < list.length; i++) {
      employees.add(Employee.fromJson(list[i]));
    }

    return employees;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}
