import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'dart:convert';
import '../../models/Appointment.dart';
import 'package:flutter/services.dart';
import 'package:loader_overlay/loader_overlay.dart';
import '../../models/Service.dart';
import '../../models/Customer.dart';
import 'package:search_choices/search_choices.dart';
import 'dart:ui' as ui;

class AppointmentCompletedViewScreen extends StatefulWidget {
  static Future<String?> show(
      BuildContext context, Appointment appointment) async {
    return await Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => AppointmentCompletedViewScreen(
        appointment: appointment,
      ),
      // fullscreenDialog: true,
    ));
  }

  const AppointmentCompletedViewScreen(
      {Key? key, required Appointment this.appointment})
      : super(key: key);

  final Appointment appointment;

  @override
  _AppointmentCompletedViewScreenState createState() =>
      _AppointmentCompletedViewScreenState();
}

class _AppointmentCompletedViewScreenState
    extends State<AppointmentCompletedViewScreen> {
  final ButtonStyle style = ElevatedButton.styleFrom(
    padding: EdgeInsets.symmetric(horizontal: 140, vertical: 16),
    textStyle: const TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
    shape: RoundedRectangleBorder(
        // borderRadius: BorderRadius.circular(28),
        ),
  );

  bool _isLoaderVisible = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Appointment details'),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: LoaderOverlay(
        child: ListView(
          children: [
            SizedBox(
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                padding:
                    const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black26,
                      width: 1.0,
                    ),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.info,
                      color: Colors.black54,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 0.0),
                    ),
                    Text(
                      widget.appointment.service.name,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black87,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                padding:
                    const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black26,
                      width: 1.0,
                    ),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.attach_money,
                      color: Colors.black54,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 0.0),
                    ),
                    Text(
                      widget.appointment.service.price.toString(),
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black87,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                padding:
                    const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black26,
                      width: 1.0,
                    ),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.face,
                      color: Colors.black54,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 0.0),
                    ),
                    Text(
                      widget.appointment.customer.fullName,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black87,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                padding:
                    const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black26,
                      width: 1.0,
                    ),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.manage_accounts,
                      color: Colors.black54,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 0.0),
                    ),
                    Text(
                      widget.appointment.employee.fullName,
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black87,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
                padding:
                    const EdgeInsets.symmetric(horizontal: 5.0, vertical: 10.0),
                decoration: BoxDecoration(
                  border: Border(
                    bottom: BorderSide(
                      color: Colors.black26,
                      width: 1.0,
                    ),
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Icon(
                      Icons.event,
                      color: Colors.black54,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5.0, vertical: 0.0),
                    ),
                    Text(
                      DateFormat('yyyy/MM/dd – hh:mm a')
                          .format(widget.appointment.fromTime),
                      style: TextStyle(
                        fontSize: 18,
                        color: Colors.black87,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Padding(
              padding:
                  const EdgeInsets.symmetric(horizontal: 0.0, vertical: 10.0),
            ),
            ElevatedButton(
              style: style,
              onPressed: () => Navigator.of(context).pop(),
              child: const Text('OK'),
            ),
          ],
        ),
      ),
    );
  }
}
