import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:service_management_app/home/appointments/appointment_completed_view_page.dart';
import 'package:service_management_app/home/appointments/empty_content.dart';
import 'package:service_management_app/models/Employee.dart';
import 'dart:convert';
import '../../models/Appointment.dart';
import 'package:intl/intl.dart';

import 'appointment_add_page.dart';
import 'appointment_view_page.dart';

class AppointmentScreen extends StatefulWidget {
  const AppointmentScreen({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AppointmentScreenState();
}

class _AppointmentScreenState extends State<AppointmentScreen> {
  Object tab1 = Object();
  Object tab2 = Object();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Appointments'),
            actions: <Widget>[
              Padding(
                  padding: EdgeInsets.only(right: 20.0),
                  child: GestureDetector(
                    onTap: () {
                      AppointmentAddScreen.show(context).then((value) {
                        if (value != null) {
                          setState(() {
                            tab1 = Object();
                          });
                          print(
                              "back to service page from service edit page: $value");
                        }
                      });
                    },
                    child: Icon(
                      Icons.add,
                      size: 26.0,
                    ),
                  )),
            ],
            bottom: const TabBar(
              tabs: [
                Tab(text: 'New'),
                Tab(text: 'Completed'),
                // Tab(icon: Icon(Icons.directions_bike)),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              Center(
                child: BodyLayout(
                  parentContext: context,
                  status: "new",
                  key: ValueKey<Object>(tab1),
                ),
              ),
              Center(
                child: BodyLayout(
                  parentContext: context,
                  status: "done",
                  key: ValueKey<Object>(tab2),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class BodyLayout extends StatefulWidget {
  const BodyLayout({
    Key? key,
    required BuildContext this.parentContext,
    required String this.status,
    // required int this.state,
  }) : super(key: key);

  final BuildContext parentContext;
  final String status;
  // final state;

  @override
  createState() => _MyListScreenState();
}

class _MyListScreenState extends State<BodyLayout> {
  var _appointments = <Appointment>[];

  _getAppointments() {
    fetchAppointments(widget.status).then((appointments) {
      setState(() {
        _appointments = appointments;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getAppointments();
    print("_MyListScreenState initState");
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_appointments.length == 0) {
      return EmptyContent();
    }

    return ListView.builder(
      itemCount: _appointments.length,
      itemBuilder: (context, index) {
        Appointment appointment = _appointments[index];

        return ListTile(
          title: Text(appointment.service.name +
              " for " +
              appointment.customer.fullName),
          subtitle: Text("at " +
              DateFormat('yyyy/MM/dd – hh:mm a')
                  .format(_appointments[index].fromTime) +
              "\nby " +
              appointment.employee.fullName),
          leading: Icon(Icons.event_outlined),
          trailing: Icon(Icons.keyboard_arrow_right),
          contentPadding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
          onTap: () {
            if (widget.status == "done") {
              AppointmentCompletedViewScreen.show(
                      widget.parentContext, appointment)
                  .then((value) {
                if (value != null) {
                  setState(() {
                    _getAppointments();
                  });
                  print("back to service page from service add page: $value");
                }
              });
            } else {
              AppointmentViewScreen.show(widget.parentContext, appointment)
                  .then((value) {
                if (value != null) {
                  setState(() {
                    _getAppointments();
                  });
                  print("back to service page from service add page: $value");
                }
              });
            }
          },
        );
      },
    );
  }
}

Future<List<Appointment>> fetchAppointments(String status) async {
  List<Appointment> appointments = [];

  String apiEndpoint = "http://localhost:8080/api/appointments?status=$status";

  final response = await http.get(Uri.parse(apiEndpoint));

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print(response.body);
    List<dynamic> list = json.decode(response.body);

    for (var i = 0; i < list.length; i++) {
      appointments.add(Appointment.fromJson(list[i]));
    }

    return appointments;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}
