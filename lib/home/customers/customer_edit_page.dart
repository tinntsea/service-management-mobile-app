import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../../models/Customer.dart';
import 'package:flutter/services.dart';
import 'package:loader_overlay/loader_overlay.dart';

class CustomerEditScreen extends StatefulWidget {
  static Future<String?> show(BuildContext context, Customer customer) async {
    return await Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => CustomerEditScreen(
        customer: customer,
      ),
      // fullscreenDialog: true,
    ));
  }

  const CustomerEditScreen({Key? key, required Customer this.customer})
      : super(key: key);

  final Customer customer;

  @override
  _CustomerEditScreenState createState() => _CustomerEditScreenState();
}

class _CustomerEditScreenState extends State<CustomerEditScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Customer Details'),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: LoaderOverlay(
        // useDefaultLoading: false,
        // overlayWidget: Center(
        //   child: SpinKitCubeGrid(
        //     color: Colors.red,
        //     size: 50.0,
        //   ),
        // ),
        // overlayOpacity: 0.8,
        child: MyCustomForm(
          customer: widget.customer,
        ),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key, required Customer this.customer})
      : super(key: key);

  final Customer customer;

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  final _nameInputController = TextEditingController();
  final _phoneInputController = TextEditingController();

  bool _isLoaderVisible = false;

  @override
  void initState() {
    _nameInputController.text = widget.customer.fullName;
    _phoneInputController.text = widget.customer.phoneNumber;
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _nameInputController.dispose();
    _phoneInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(
      padding: EdgeInsets.symmetric(horizontal: 140, vertical: 16),
      textStyle: const TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
      // shape: RoundedRectangleBorder(
      //   borderRadius: BorderRadius.circular(28),
      // ),
    );

    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextFormField(
              controller: _nameInputController,
              decoration: const InputDecoration(
                hintText: 'Enter customer name',
                prefixIcon: Icon(Icons.sell_outlined),
              ),

              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Customer name should not be null';
                }
                return null;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextFormField(
              controller: _phoneInputController,
              decoration: const InputDecoration(
                hintText: 'Enter phone number',
                prefixIcon: Icon(Icons.attach_money_outlined),
              ),
              keyboardType: TextInputType.number,
              // The validator receives the text that the user has entered.
              inputFormatters: <TextInputFormatter>[
                FilteringTextInputFormatter.allow(RegExp(r'\+?[0-9]*')),
              ],
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Please enter phone number';
                }
                return null;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
            child: Center(
              child: ElevatedButton(
                style: style,
                onPressed: () async {
                  // Validate returns true if the form is valid, or false otherwise.
                  if (_formKey.currentState!.validate()) {
                    context.loaderOverlay.show();
                    setState(() {
                      _isLoaderVisible = context.loaderOverlay.visible;
                    });

                    await updateCustomer(
                      widget.customer,
                      _nameInputController.text,
                      _phoneInputController.text,
                    );
                    await Future.delayed(Duration(seconds: 1));

                    if (_isLoaderVisible) {
                      context.loaderOverlay.hide();
                    }
                    setState(() {
                      _isLoaderVisible = context.loaderOverlay.visible;
                    });

                    ScaffoldMessenger.of(context)
                        .showSnackBar(
                          const SnackBar(
                            content: Text('The customer has been updated.'),
                            duration: const Duration(milliseconds: 500),
                            backgroundColor: Colors.green,
                          ),
                        )
                        .closed
                        .then((_) => Navigator.of(context).pop("test"));
                  }
                },
                child: const Text('Update'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<Customer> updateCustomer(
    Customer customer, String name, String phone) async {
  int id = customer.id;
  Map data = {
    'name': name,
    'phone': phone,
  };

  String apiEndpoint = "http://localhost:8080/api/customers/$id";

  final response = await http.put(
    Uri.parse(apiEndpoint),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(data),
  );

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 201 || response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    print(response.body);
    return Customer.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to update customer.');
  }
}
