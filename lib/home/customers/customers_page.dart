import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../../models/Customer.dart';
import 'customer_add_page.dart';
import 'customer_edit_page.dart';

class CustomerScreen extends StatefulWidget {
  const CustomerScreen({Key? key}) : super(key: key);

  @override
  createState() => _MyListScreenState();
}

class _MyListScreenState extends State<CustomerScreen> {
  var _customers = <Customer>[];

  _getCustomers() {
    fetchCustomers().then((data) {
      setState(() {
        _customers = data;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getCustomers();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Customers'),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) => CustomerAddScreen()),
                  // );
                  CustomerAddScreen.show(context).then((value) {
                    if (value != null) {
                      setState(() {
                        _getCustomers();
                      });
                      print(
                          "back to service page from service add page: $value");
                    }
                  });
                },
                child: Icon(
                  Icons.add,
                  size: 26.0,
                ),
              )),
        ],
      ),
      body: ListView.separated(
        itemCount: _customers.length,
        separatorBuilder: (BuildContext context, int index) {
          return const Divider(
            height: 0,
          );
        },
        itemBuilder: (context, index) {
          Customer customer = _customers[index];
          return ListTile(
            title: Text(customer.fullName, style: TextStyle(fontSize: 16)),
            subtitle: Text(customer.phoneNumber),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {
              CustomerEditScreen.show(context, customer).then((value) {
                if (value != null) {
                  setState(() {
                    _getCustomers();
                  });
                  print("back to service page from service edit page: $value");
                }
              });
            },
          );
        },
      ),
    );
  }
}

Future<List<Customer>> fetchCustomers() async {
  List<Customer> customers = [];

  String apiEndpoint = "http://localhost:8080/api/customers";

  final response = await http.get(Uri.parse(apiEndpoint));

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print(response.body);
    List<dynamic> list = json.decode(response.body);

    for (var i = 0; i < list.length; i++) {
      customers.add(Customer.fromJson(list[i]));
    }

    return customers;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}
