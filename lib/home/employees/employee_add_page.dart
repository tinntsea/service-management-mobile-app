import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import '../../models/Employee.dart';
// import 'package:flutter/services.dart';
import 'package:loader_overlay/loader_overlay.dart';

class EmployeeAddScreen extends StatefulWidget {
  static Future<String?> show(BuildContext context) async {
    return await Navigator.of(context).push(MaterialPageRoute(
      builder: (context) => EmployeeAddScreen(),
      fullscreenDialog: true,
    ));
  }

  @override
  _EmployeeAddScreenState createState() => _EmployeeAddScreenState();
}

class _EmployeeAddScreenState extends State<EmployeeAddScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Add a new employee'),
        leading: IconButton(
          icon: Icon(Icons.close),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: LoaderOverlay(
        // useDefaultLoading: false,
        // overlayWidget: Center(
        //   child: SpinKitCubeGrid(
        //     color: Colors.red,
        //     size: 50.0,
        //   ),
        // ),
        // overlayOpacity: 0.8,
        child: MyCustomForm(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm extends StatefulWidget {
  const MyCustomForm({Key? key}) : super(key: key);

  @override
  MyCustomFormState createState() {
    return MyCustomFormState();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState extends State<MyCustomForm> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  final _formKey = GlobalKey<FormState>();

  final _nameInputController = TextEditingController();

  bool _isLoaderVisible = false;

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    _nameInputController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final ButtonStyle style = ElevatedButton.styleFrom(
      padding: EdgeInsets.symmetric(horizontal: 140, vertical: 16),
      textStyle: const TextStyle(fontSize: 24, fontWeight: FontWeight.w500),
      // shape: RoundedRectangleBorder(
      //   borderRadius: BorderRadius.circular(28),
      // ),
    );

    // Build a Form widget using the _formKey created above.
    return Form(
      key: _formKey,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextFormField(
              controller: _nameInputController,
              decoration: const InputDecoration(
                hintText: 'Enter employee name',
                prefixIcon: Icon(Icons.sell_outlined),
                // border: OutlineInputBorder(
                //     borderRadius: BorderRadius.all(
                //   Radius.circular(30.0),
                // )),
                // enabledBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.black45, width: 1.0),
                //   borderRadius: BorderRadius.all(Radius.circular(30.0)),
                // ),
                // focusedBorder: OutlineInputBorder(
                //   borderSide: BorderSide(color: Colors.black45, width: 1.0),
                //   borderRadius: BorderRadius.all(Radius.circular(30.0)),
                // ),
                // filled: true,
                // fillColor: Colors.black45, //Palette.inputFillColor,
              ),

              // The validator receives the text that the user has entered.
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Employee name should not be null';
                }
                return null;
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: 8, vertical: 10),
            child: Center(
              child: ElevatedButton(
                style: style,
                onPressed: () async {
                  // Validate returns true if the form is valid, or false otherwise.
                  if (_formKey.currentState!.validate()) {
                    context.loaderOverlay.show();
                    setState(() {
                      _isLoaderVisible = context.loaderOverlay.visible;
                    });

                    await createEmployee(_nameInputController.text);
                    await Future.delayed(Duration(seconds: 1));

                    if (_isLoaderVisible) {
                      context.loaderOverlay.hide();
                    }
                    setState(() {
                      _isLoaderVisible = context.loaderOverlay.visible;
                    });

                    ScaffoldMessenger.of(context)
                        .showSnackBar(
                          const SnackBar(
                            content: Text('New employee has been created.'),
                            duration: const Duration(milliseconds: 500),
                            backgroundColor: Colors.green,
                          ),
                        )
                        .closed
                        .then((_) => Navigator.of(context).pop("test"));
                  }
                },
                child: const Text('Submit'),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Future<Employee> createEmployee(String name) async {
  Map data = {
    'name': name,
  };
  // final response = await http.post(
  //   Uri.parse('http://localhost:8080/api/employees'),
  //   headers: <String, String>{
  //     'Content-Type': 'application/json; charset=UTF-8',
  //   },
  //   body: jsonEncode(data),
  // );

  String apiEndpoint = "http://localhost:8080/api/employees";

  final response = await http.post(
    Uri.parse(apiEndpoint),
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(data),
  );

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 201 || response.statusCode == 200) {
    // If the server did return a 201 CREATED response,
    // then parse the JSON.
    print(response.body);
    return Employee.fromJson(jsonDecode(response.body));
  } else {
    // If the server did not return a 201 CREATED response,
    // then throw an exception.
    throw Exception('Failed to create employee.');
  }
}
