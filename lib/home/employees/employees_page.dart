import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:service_management_app/home/employees/employee_edit_page.dart';
import 'dart:convert';
import '../../models/Employee.dart';
import 'employee_add_page.dart';

class EmployeeScreen extends StatefulWidget {
  const EmployeeScreen({Key? key}) : super(key: key);

  @override
  createState() => _MyListScreenState();
}

class _MyListScreenState extends State<EmployeeScreen> {
  var _employees = <Employee>[];

  _getEmployees() {
    fetchEmployees().then((data) {
      setState(() {
        _employees = data;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getEmployees();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Employees'),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //       builder: (context) => EmployeeAddScreen()),
                  // );

                  EmployeeAddScreen.show(context).then((value) {
                    if (value != null) {
                      setState(() {
                        _getEmployees();
                      });
                      print(
                          "back to service page from employee add page: $value");
                    }
                  });
                },
                child: Icon(
                  Icons.add,
                  size: 26.0,
                ),
              )),
        ],
      ),
      body: ListView.separated(
        itemCount: _employees.length,
        separatorBuilder: (BuildContext context, int index) {
          return const Divider(
            height: 0,
          );
        },
        itemBuilder: (context, index) {
          Employee employee = _employees[index];

          return ListTile(
            title: Text(employee.fullName),
            trailing: Icon(Icons.keyboard_arrow_right),
            onTap: () {
              EmployeeEditScreen.show(context, employee).then((value) {
                if (value != null) {
                  setState(() {
                    _getEmployees();
                  });
                  print("back to service page from service edit page: $value");
                }
              });
            },
          );
        },
      ),
    );
  }
}

Future<List<Employee>> fetchEmployees() async {
  List<Employee> employees = [];

  String apiEndpoint = "http://localhost:8080/api/employees";

  final response = await http.get(Uri.parse(apiEndpoint));

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print(response.body);
    List<dynamic> list = json.decode(response.body);

    for (var i = 0; i < list.length; i++) {
      employees.add(Employee.fromJson(list[i]));
    }

    return employees;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}
