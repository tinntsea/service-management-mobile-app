import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:service_management_app/home/services/services_add_page.dart';
import 'package:service_management_app/home/services/services_edit_page.dart';
import 'dart:convert';
import '../../models/Service.dart';

class ServiceScreen extends StatefulWidget {
  // const ServiceScreen({Key? key}) : super(key: key);

  @override
  createState() => _MyListScreenState();
}

// class BodyLayout extends StatefulWidget {
//   @override
//   createState() => _MyListScreenState();

// @override
// Widget build(BuildContext context) {
//   return _myListView(context);
// }
// }

class _MyListScreenState extends State {
  var _services = <Service>[];

  _getService() {
    fetchServices().then((services) {
      setState(() {
        _services = services;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getService();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Services'),
        actions: <Widget>[
          Padding(
              padding: EdgeInsets.only(right: 20.0),
              child: GestureDetector(
                onTap: () {
                  ServiceAddScreen.show(context).then((value) {
                    if (value != null) {
                      setState(() {
                        _getService();
                      });
                      print(
                          "back to service page from service add page: $value");
                    }
                  });
                },
                child: Icon(
                  Icons.add,
                  size: 26.0,
                ),
              )),
        ],
      ),
      body: ListView.separated(
        itemCount: _services.length,
        separatorBuilder: (BuildContext context, int index) {
          return const Divider(
            height: 0,
          );
        },
        itemBuilder: (context, index) {
          Service service = _services[index];
          return ListTile(
            title: Text(service.name, style: TextStyle(fontSize: 16)),
            subtitle: Text('\$' + service.price.toString()),
            trailing: Icon(Icons.keyboard_arrow_right),
            // minLeadingWidth: 0,
            // minVerticalPadding: 0,
            // contentPadding:
            //     EdgeInsets.symmetric(vertical: 0.0, horizontal: 0.0),
            onTap: () {
              ServiceEditScreen.show(context, service).then((value) {
                if (value != null) {
                  setState(() {
                    _getService();
                  });
                  print("back to service page from service edit page: $value");
                }
              });
            },
          );
        },
      ),
    );
  }
}

Future<List<Service>> fetchServices() async {
  List<Service> services = [];

  String apiEndpoint = "http://localhost:8080/api/services";

  final response = await http.get(Uri.parse(apiEndpoint));

  print("Calling to " + apiEndpoint);

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    print(response.body);

    List<dynamic> list = json.decode(response.body);

    for (var i = 0; i < list.length; i++) {
      services.add(Service.fromJson(list[i]));
    }

    return services;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}
