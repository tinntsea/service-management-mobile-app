

import 'package:flutter/material.dart';

enum TabItem {
  Appointments,
  Settings,
}

const Map<TabItem, String> tabName = {
  TabItem.Appointments: 'Appointments',
  TabItem.Settings: 'Settings',
};

// const Map<TabItem, MaterialColor> activaTabColor = {
//   TabItem.Appointments: 'Appointments',
//   TabItem.Settings: 'Settings',
// };