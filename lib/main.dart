/// Flutter code sample for BottomNavigationBar

// This example shows a [BottomNavigationBar] as it is used within a [Scaffold]
// widget. The [BottomNavigationBar] has three [BottomNavigationBarItem]
// widgets, which means it defaults to [BottomNavigationBarType.fixed], and
// the [currentIndex] is set to index 0. The selected item is
// amber. The `_onItemTapped` function changes the selected item's index
// and displays a corresponding message in the center of the [Scaffold].

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:service_management_app/home/tab_item.dart';
// import 'package:service_management_app/screens/AdministrationScreen.dart';
import 'package:service_management_app/services/api.dart';
import 'dart:convert';
import 'home/administration/administration_page.dart';
import 'home/appointments/appointments_page.dart';
import 'models/Service.dart';

void main() => runApp(const MyApp());

/// This is the main application widget.
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: MyStatefulWidget(),
    );
  }
}

/// This is the stateful widget that the main application instantiates.
class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  var _currentTab = TabItem.Appointments;

  int _selectedIndex = 0;

  List<GlobalKey<NavigatorState>> navigatorKeys = [
    GlobalKey<NavigatorState>(),
    GlobalKey<NavigatorState>(),
  ];

  // final Map<TabItem, GlobalKey<NavigatorState>> navigatorKeys = {
  //   TabItem.jobs: GlobalKey<NavigatorState>(),
  //   TabItem.entries: GlobalKey<NavigatorState>(),
  //   TabItem.account: GlobalKey<NavigatorState>(),
  // };

  // static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _widgetOptions = <Widget>[
    // HomePage(),
    Provider<SmsApi>(
      create: (context) => SmsApiImpl(),
      child: AppointmentScreen(),
    ),
    // ServiceScreen(),
    // CustomerScreen(),
    // EmployeeScreen(),
    AdministrationScreen(),
    // BodyLayoutStateless(),
    // Text(
    //   'Staff',
    //   style: optionStyle,
    // ),
  ];

  void _onItemTapped(int index) {
    if (index == _selectedIndex) {
      navigatorKeys[index].currentState!.popUntil((route) => route.isFirst);
    } else {
      setState(() {
        _selectedIndex = index;
        // _currentTab = tabItem;
      });
    }

    // Navigator.push(context, MaterialPageRoute(builder: (context) {
    //   return const ServiceScreen();
    // }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('Service Management System'),
      // ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        // backgroundColor: Colors.blue,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.event_available_outlined),
            label: 'Appointments',
          ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.settings),
          //   label: 'Service',
          // ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.people),
          //   label: 'Customers',
          // ),
          // BottomNavigationBarItem(
          //   icon: Icon(Icons.engineering),
          //   label: 'Staff',
          // ),
          BottomNavigationBarItem(
            icon: Icon(Icons.engineering),
            label: 'Administration',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.blue[800],
        onTap: _onItemTapped,
      ),
    );
  }

  // BottomNavigationBarItem _buildItem(int index) {
  //
  // }
}

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // bottomNavigationBar: BottomNavigationBar(
      //   backgroundColor: Colors.orange,
      //   items: [
      //     BottomNavigationBarItem(icon: Icon(Icons.call), label: 'Call'),
      //     BottomNavigationBarItem(icon: Icon(Icons.message), label: 'Message'),
      //   ],
      // ),
      body: ListView(
        children: const <Widget>[
          ListTile(
            leading: Icon(Icons.map),
            title: Text('Map'),
          ),
          ListTile(
            leading: Icon(Icons.photo_album),
            title: Text('Album'),
          ),
          ListTile(
            leading: Icon(Icons.phone),
            title: Text('Phone'),
          ),
        ],
      ),
    );
  }
}

class BodyLayout extends StatefulWidget {
  @override
  createState() => _MyListScreenState();

  // @override
  // Widget build(BuildContext context) {
  //   return _myListView(context);
  // }
}

class BodyLayoutStateless extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _myListView(context);
  }
}

class _MyListScreenState extends State {
  var _services = <Service>[];

  _getService() {
    fetchServices().then((services) {
      setState(() {
        _services = services;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getService();
  }

  @override
  dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: _services.length,
      itemBuilder: (context, index) {
        return ListTile(
          title: Text(_services[index].name),
          trailing: Icon(Icons.keyboard_arrow_right),
        );
      },
    );
  }
}

// replace this function with the code in the examples
Widget _myListView(BuildContext context) {
  // backing data
  final europeanCountries = [
    'Albania',
    'Andorra',
    'Armenia',
    'Austria',
    'Azerbaijan',
    'Belarus',
    'Belgium',
    'Bosnia and Herzegovina',
    'Bulgaria',
    'Croatia',
    'Cyprus',
    'Czech Republic',
    'Denmark',
    'Estonia',
    'Finland',
    'France',
    'Georgia',
    'Germany',
    'Greece',
    'Hungary',
    'Iceland',
    'Ireland',
    'Italy',
    'Kazakhstan',
    'Kosovo',
    'Latvia',
    'Liechtenstein',
    'Lithuania',
    'Luxembourg',
    'Macedonia',
    'Malta',
    'Moldova',
    'Monaco',
    'Montenegro',
    'Netherlands',
    'Norway',
    'Poland',
    'Portugal',
    'Romania',
    'Russia',
    'San Marino',
    'Serbia',
    'Slovakia',
    'Slovenia',
    'Spain',
    'Sweden',
    'Switzerland',
    'Turkey',
    'Ukraine',
    'United Kingdom',
    'Vatican City'
  ];

  return ListView.builder(
    itemCount: europeanCountries.length,
    itemBuilder: (context, index) {
      return ListTile(
        title: Text(europeanCountries[index]),
        trailing: Icon(Icons.keyboard_arrow_right),
      );
    },
  );
}

Future<List<Service>> fetchServices() async {
  List<Service> services = [];

  final response =
      await http.get(Uri.parse('http://localhost:8080/api/service/all'));

  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    List<dynamic> list = json.decode(response.body);

    for (var i = 0; i < list.length; i++) {
      services.add(Service.fromJson(list[i]));
    }

    return services;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}

// class Service {
//   // final int userId;
//   final int id;
//   final String name;
//   final double price;
//
//   Service({
//     required this.id,
//     required this.name,
//     required this.price,
//   });
//
//   factory Service.fromJson(Map<String, dynamic> json) {
//     return Service(
//       id: json['id'],
//       name: json['name'],
//       price: json['price'],
//     );
//   }
// }
