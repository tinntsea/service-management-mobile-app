import 'Employee.dart';
import 'Customer.dart';
import 'Service.dart';

class Appointment {
  // final int userId;
  final int id;
  final Service service;
  final Customer customer;
  final Employee employee;
  final DateTime fromTime;
  final DateTime toTime;

  Appointment({
    required this.id,
    required this.service,
    required this.customer,
    required this.employee,
    required this.fromTime,
    required this.toTime,
  });

  factory Appointment.fromJson(Map<String, dynamic> json) {
    return Appointment(
      id: json['id'],
      service: Service.fromJson(json['service']),
      customer: Customer.fromJson(json['customer']),
      employee: Employee.fromJson(json['employee']),
      fromTime: DateTime.parse(json['fromTime']),
      toTime: DateTime.parse(json['toTime']),
    );
  }
}