class Customer {
  // final int userId;
  final int id;
  final String fullName;
  final String phoneNumber;

  Customer({
    required this.id,
    required this.fullName,
    required this.phoneNumber,
  });

  factory Customer.fromJson(Map<String, dynamic> json) {
    return Customer(
      id: json['id'],
      fullName: json['fullName'],
      phoneNumber: json['phoneNumber'],
    );
  }

  @override
  String toString() {
    return fullName;
  }
}