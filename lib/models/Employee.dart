class Employee {
  final int id;
  final String fullName;

  Employee({
    required this.id,
    required this.fullName,
  });

  factory Employee.fromJson(Map<String, dynamic> json) {
    return Employee(
      id: json['id'],
      fullName: json['fullName'],
    );
  }
}