import 'dart:convert';

import 'package:service_management_app/models/Appointment.dart';
import 'package:http/http.dart' as http;

abstract class SmsApi {
  Future<List<Appointment>> getAppointments();
}

class SmsApiImpl implements SmsApi {
  Future<List<Appointment>> getAppointments() async {
    List<Appointment> appointments = [];

    String apiEndpoint = "http://localhost:8080/api/appointments";

    final response = await http.get(Uri.parse(apiEndpoint));

    print("Calling to " + apiEndpoint);

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      print(response.body);
      List<dynamic> list = json.decode(response.body);

      for (var i = 0; i < list.length; i++) {
        appointments.add(Appointment.fromJson(list[i]));
      }

      return appointments;
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load album');
    }
  }
}
